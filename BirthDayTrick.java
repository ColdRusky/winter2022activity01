import java.util.Scanner;

public class BirthDayTrick {

public static void main(String[] args) {

//Making a Scanner
Scanner scanner = new Scanner(System.in);
	
	//Asking the user's birthMonth
	System.out.println("What is your birth month? ");
	int birthMonth = scanner.nextInt();
	
	//Asking the user's birthDay
	System.out.println("What is your birth day? ");
	int birthDay = scanner.nextInt();
	
	//Assigning the number 7 to the magicNumber variable and displaying it to the user
	double magicNumber = 7;
	System.out.println("Your magic number is: "+ magicNumber);
	
	//The program that keeps updating the magicNumber variable
	magicNumber = magicNumber * birthMonth;
	magicNumber = magicNumber - 1;
	magicNumber = magicNumber * 13;
	magicNumber = magicNumber + birthDay;
	magicNumber = magicNumber + 3;
	magicNumber = magicNumber * 11;
	magicNumber = magicNumber - birthMonth;
	magicNumber = magicNumber - birthDay;
	magicNumber = magicNumber / 10;
	magicNumber = magicNumber + 11;
	magicNumber = magicNumber / 100;
	
	//Display the newly created magic number to the user
	System.out.println("Your new magic number is: "+ magicNumber);

}

}